import express from 'express';

const router = express.Router();

router.get('/', (req, res) => {
  res.send('Hello, World! I am a Node.js server.');
});

export default router;
